const opts = {
   base: {
    demand: true,
    alias: 'b'
  },
  limit: {
    alias: 'l',
    default: 10
  } 
}

const argv = require('yargs')
  .command('list', 'Prints the multiply table.', opts)
  .command('create', 'Creates a multiply table and stores this in a .txt file.', opts)
  .help()
  .argv;

module.exports = {
  argv
}
