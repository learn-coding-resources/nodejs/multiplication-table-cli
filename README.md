# Create, store in .txt files, and list multiplication tables, using a terminal prompt.

### Packages:

- [Node's fs](https://nodejs.org/api/fs.html)
- [Yargs](https://www.npmjs.com/package/yargs)
- [Colors](https://www.npmjs.com/package/colors)

### INSTALL

1. **Open your terminal and Clone:**
```
git clone git@gitlab.com:learn-coding-resources/nodejs/multiplication-table-cli.git
```

2. **Go to inside folder:**
```
cd multiplication-table-cli
```

3. **Install dependencies:**

```
npm install
```

### HOW TO USE:

```
$ node app.js [command]

Commands:
  app.js list    Prints the multiply table.
  app.js create  Creates a multiply table and stores this in a .txt file.

Options:
  --version  Show version number                                       [boolean]
  --help     Show help                                                 [boolean]
```



