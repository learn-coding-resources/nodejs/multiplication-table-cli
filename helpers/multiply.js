const fs = require('fs');
const colors = require('colors');

let listTable = (base, limit = 10) => {
  const heading = `Table of ${base}`;
  const headingLength = heading.length;
  console.log('='.repeat(headingLength).green);
  console.log(heading.green);
  console.log('='.repeat(headingLength).green);
  for(let i = 1; i <= limit; i++) {
    console.log(`${base} * ${i} = ${base * i}`.inverse);
  }
}

let createFile = (base, limit = 10) => {
  return new Promise( (resolve, reject) => {

    if( !Number(base) && !Number(limit) ) {
      reject(`Error: You typed --base: "${base}", --limit: "${limit}", and the value for both args should be number.\nInsert only numbers, please.`.red);
      return;
    }

    let data = '';

    for( let i = 1; i <= limit; i++ ) {
      data += `${base} * ${i} = ${base * i}\n`;
    }

    fs.writeFile(`./filestorage/table-${base}-to-${limit}.txt`, data, (err) => {
      if (err)
        reject(err.red)
      else
        resolve(`table-${base}-to-${limit}.txt`);
    });
  } )
}

module.exports = {
  createFile,
  listTable
}

