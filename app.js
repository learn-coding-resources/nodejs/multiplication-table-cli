const argv = require('./config/yargs').argv;
const colors = require('colors/safe');

const {createFile, listTable} = require('./helpers/multiply');

let command = argv._[0];

switch (command) {
  case 'list':
    listTable(argv.base, argv.limit);
    break;
  case 'create':
    createFile(argv.base, argv.limit)
      .then( file => console.log('File created:', colors.green(file)) )
      .catch( err => console.log(err)); 
    break;
  default:
    console.log('Command not found');
}


console.log(argv);
//console.log(argv.base);

// let param = argv[2];
// let base = param.split('=')[1];

/*
  */
